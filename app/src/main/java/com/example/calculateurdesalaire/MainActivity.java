package com.example.calculateurdesalaire;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private TextView textViewMainTitle;
    private EditText grossSalaryInsertionEditText;
    private Button mainActivityValidationButton;
    private TextView netSalaryTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textViewMainTitle = (TextView)findViewById(R.id.main_activity_main_title);
        grossSalaryInsertionEditText = (EditText)findViewById(R.id.gross_salary_insertion_edit_text);
        mainActivityValidationButton = (Button)findViewById(R.id.main_activity_validation_button);
        netSalaryTextView = (TextView)findViewById(R.id.net_salary_text_view);
        mainActivityValidationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputGrossSalaryTextStringValue = grossSalaryInsertionEditText.getText().toString();
                float inputGrossSalary;
                try {
                    inputGrossSalary = Float.parseFloat(inputGrossSalaryTextStringValue);
                    float netSalary = 0.8F * inputGrossSalary;
                    String netSalaryTextViewDisplayText = getString(R.string.net_salary_result_string, inputGrossSalary, netSalary);
                    netSalaryTextView.setText(netSalaryTextViewDisplayText);
                }
                catch (NumberFormatException e) {
                    netSalaryTextView.setText(R.string.number_format_exception);
                }
            }
        });
    }
}